# Kafka REST Proxy Kubernetes

This repo contains a deployment file for setting up Kafka REST proxy in your Kubernetes cluster.  
**Prerequistes: you must have a properties configuration file with parameters like  bootstrap server, username, password, sasl method,etc.**

Steps:  
1. Create a secret with configuration for the rest proxy:
`kubectl create secret generic kafka-rest-config --from-file kafka-rest-config.properties`
2. Deploy the rest proxy:
`curl -s https://gitlab.com/atkozhuharov/kafka-rest-proxy-kubernetes/raw/master/kafka-proxy-deployment.yaml | kubectl apply -f -`